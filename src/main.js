import Vue from 'vue'
import App from './App.vue'
import Vueaxios from 'vue-axios'
import axios from 'axios'
import VueRouter from 'vue-router'
import routes from '@/router/index.js'
//import home from './home.vue'
Vue.config.productionTip = false;
Vue.use(VueRouter);

new Vue({
   routes,
   render: h => h(App)
}).$mount('#app');

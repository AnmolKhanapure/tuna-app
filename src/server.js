var express = require('express');
var path = require('path');
var bodyparser = require('body-parser');
var http = require('http');
var cors = require('cors');
var app = express();
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
require('@/router/index.js');
app.use(cors());
var port = process.env.PORT || 4000;
app.listen(()=>{
    console.log("listening on" + port);
})

